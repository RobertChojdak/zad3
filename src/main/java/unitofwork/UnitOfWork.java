package unitofwork;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import model.Entity;
import model.EntityState;

public class UnitOfWork implements IUnitOfWork{

	private Connection connection;
	
	private Map<Entity, IUnitOfWorkRepository> entities = 
			new LinkedHashMap<Entity, IUnitOfWorkRepository>();
	
	public UnitOfWork(Connection connection) {
		this.connection = connection;
		try {
			connection.setAutoCommit(false);
		}catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	public void commit() {
		for(Entity entity:entities.keySet()) {
			if(entity.getState() == EntityState.New) {
				entities.get(entity).persistAdd(entity);
			}
			else if(entity.getState() == EntityState.Changed) {
				entities.get(entity).persistUpdate(entity);
			}
			else if(entity.getState() == EntityState.Deleted) {
				entities.get(entity).persistDelete(entity);
			}
			else break;
		}
		
		try {
			connection.commit();
			entities.clear();
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}

	public void rollback() {
		entities.clear();
	}

	public void markAsNew(Entity entity, IUnitOfWorkRepository repository) {
		entity.setState(EntityState.New);
		entities.put(entity, repository);
		
	}

	public void markAsDirty(Entity entity, IUnitOfWorkRepository repository) {
		entity.setState(EntityState.Changed);
		entities.put(entity, repository);
	}

	public void markAsDeleted(Entity entity, IUnitOfWorkRepository repository) {
		entity.setState(EntityState.Deleted);
		entities.put(entity, repository);
	}
}
