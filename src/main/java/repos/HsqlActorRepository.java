package repos;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.spi.DateFormatProvider;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import db.ActorRepository;
import db.PagingInfo;
import model.Actor;


public class HsqlActorRepository {
	private Connection connection;
	private String createActorTable = "CREATE TABLE ACTOR(ID INT IDENTITY, NAME VARCHAR(20)"
									+ ", DATE_OF_BIRTH DATE, BIOGRAPHY VARCHAR(400))";
	private String findByIdStmt = "SELECT * FROM actor WHERE id = ?";
	private String addActorStmt = "INSERT INTO ACTOR (NAME, DATE_OF_BIRTH, BIOGRAPHY) VALUES (?, ?, ?)";
	private String removeActorStmt = "DELETE FROM ACTOR WHERE id = ?";
	private String modifyActorStmt = "UPDATE ACTOR SET name = ? AND date_of_birth = ? AND biography = ? WHERE id = ?";
	private String findByNameStmt = "SELECT * FROM actor WHERE name = ?";
	private String selectByDateOfBirth = "SELECT * FROM person WHERE date_of_birth=? offset ? limit ?";
	
	public HsqlActorRepository(Connection connection) {
		this.connection = connection;
		
		try {
			ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
			boolean isTableCreated = false;
			
			while(rs.next()) {
				if(rs.getString("TABLE_NAME").equalsIgnoreCase("Actor")) {
					isTableCreated = true;
					break;
				}
			}
			if(!isTableCreated) {
				Statement createTable = connection.createStatement();
				createTable.executeUpdate(createActorTable);
			}
		}
		catch(SQLException ex) {
			ex.printStackTrace();
		}	
	}

	public Actor withId(int id) {
		Actor actor = new Actor();
		try {
			PreparedStatement findStatement = connection.prepareStatement(findByIdStmt);
			findStatement.setInt(1, id);
			ResultSet rs = findStatement.executeQuery();
			while(rs.next()) {
				actor.setId(rs.getInt("ID"));
				actor.setName(rs.getString("NAME"));
				actor.setDateOfBirth(rs.getDate("DATE_OF_BIRTH").toLocalDate());
				actor.setBiography(rs.getString("BIOGRAPHY"));
			}
		}
		catch(SQLException ex) {
			ex.printStackTrace();
		}
		return actor;
	}

	public void add(Actor entity) {
		try {
			PreparedStatement addActor = connection.prepareStatement(addActorStmt);
			addActor.setString(1, entity.getName());
			addActor.setDate(2, Date.valueOf(entity.getDateOfBirth()));
			addActor.setString(3, entity.getBiography());
			addActor.executeUpdate();
		}
		catch(SQLException ex) {
			ex.printStackTrace();
		}
		
	}

	public void remove(Actor entity) {
		try {
			PreparedStatement removeActor = connection.prepareStatement(removeActorStmt);
			removeActor.setInt(1, entity.getId());
			removeActor.executeUpdate();
		}
		catch(SQLException ex) {
			ex.printStackTrace();
		}
	}

	public void modify(Actor entity) {
		try {
			PreparedStatement modifyActor = connection.prepareStatement(modifyActorStmt);
			modifyActor.setString(1, entity.getName());
			modifyActor.setDate(2, Date.valueOf(entity.getDateOfBirth()));
			modifyActor.setString(3, entity.getBiography());
			modifyActor.setInt(4, entity.getId());
			modifyActor.executeUpdate();
		}
		catch(SQLException ex) {
			ex.printStackTrace();
		}
	}

	public Actor withName(String name) {
		Actor actor = new Actor();
		try {
			PreparedStatement findActor = connection.prepareStatement(findByNameStmt);
			findActor.setString(1, name);
			ResultSet rs = findActor.executeQuery();
			while(rs.next()) {
				actor.setId(rs.getInt("ID"));
				actor.setName(rs.getString("NAME"));
				actor.setDateOfBirth(rs.getDate("DATE_OF_BIRTH").toLocalDate());
				actor.setBiography(rs.getString("BIOGRAPHY"));
			}
		}
		catch(SQLException ex) {
			ex.printStackTrace();
		}
		return actor;
	}

	public List<Actor> withDateOfBirth(LocalDate dob, PagingInfo page) {
		List<Actor> actorsByDOB = new ArrayList();
        try {
        	PreparedStatement selectByDOB = connection.prepareStatement(selectByDateOfBirth);
        	selectByDOB.setDate(1, Date.valueOf(dob));
        	selectByDOB.setInt(2, page.getCurrentPage() * page.getSize());
        	selectByDOB.setInt(3, page.getSize());
            ResultSet rs = selectByDOB.executeQuery();
            while (rs.next()) {
            	Actor actor = new Actor();
            	actor.setId(rs.getInt("ID"));
				actor.setName(rs.getString("NAME"));
				actor.setDateOfBirth(rs.getDate("DATE_OF_BIRTH").toLocalDate());
				actor.setBiography(rs.getString("BIOGRAPHY"));
            	actorsByDOB.add(actor);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return actorsByDOB;
	}

	public void save(Actor entity) {
		// TODO Auto-generated method stub
		
	}

	public void update(Actor entity) {
		// TODO Auto-generated method stub
		
	}

	public void delete(Actor entity) {
		// TODO Auto-generated method stub
		
	}

	public Actor get(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Actor> getAll() {
		// TODO Auto-generated method stub
		return null;
	}
}
