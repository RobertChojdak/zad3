package catalogs;

import java.sql.Connection;
import repos.*;
import db.*;

public class HsqlRepositoryCatalog {

	Connection connection;
	    
	public HsqlRepositoryCatalog(Connection connection) {
		this.connection = connection;
	}

	public ActorRepository actors() {
	    return new HsqlActorRepository(connection);
	}
//	public DirectorRepository directors() {
//	    return new HsqlDirectorRepository(connection);
//	}
//	public TvSeriesRepository tvSeries() {
//	    return new HsqlTvSerieRepository(connection);
//	}
//	public SeasonRepository seasons() {
//	    return new HsqlSeasonRepository(connection);
//	}
//	public EpisodeRepository episodes() {
//	    return new HsqlEpisodeRepository(connection);
//	}
}
