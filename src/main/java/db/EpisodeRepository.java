package db;

import java.util.List;
import model.Episode;

public interface EpisodeRepository extends IRepository<Episode> {
	Episode withName();
	Episode withReleaseDate();
	List<Episode> withEpisodeNumber();
	List<Episode> withDuration();
}
