package db;

import java.time.LocalDate;
import java.util.List;
import model.Director;

public interface DirectorRepository extends IRepository<Director> {
	Director withName(String name);
	List<Director> withDateOfBirth(LocalDate dob, PagingInfo page);
}
