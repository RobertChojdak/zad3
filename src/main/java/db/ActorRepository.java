package db;

import model.Actor;
import db.PagingInfo;
import java.util.List;
import java.time.LocalDate;

public interface ActorRepository extends IRepository<Actor> {
	Actor withName(String name);
	List<Actor> withDateOfBirth(LocalDate dob, PagingInfo page);
}
