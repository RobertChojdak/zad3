package db;

import java.util.List;

import model.Actor;
import db.IRepository;

public class DummyActorRepository {
private DummyDb db;
	
	public DummyActorRepository(DummyDb db) {
		this.db = db;
	}

	public void save(Actor entity) {

		db.actors.add(entity);
		
	}

	public void update(Actor entity) {
		
	}

	public void delete(Actor entity) {

		db.actors.remove(entity);
		
	}

	
	public Actor get(int id) {

		for(Actor a: db.actors)
			if(a.getId()==id)
				return a;
		return null;
	}

	public List<Actor> getAll() {
		return db.actors;
	}
}
