package db;

import model.Season;
import model.TvSeries;

public interface TvSeriesRepository extends IRepository<TvSeries> {
	TvSeries withName();
}
