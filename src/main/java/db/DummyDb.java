package db;

import model.*;
import java.util.*;

public class DummyDb {
	List<Actor> actors = new ArrayList<Actor>();
	List<Director> directors = new ArrayList<Director>();
	List<Episode> episodes = new ArrayList<Episode>();
	List<Season> seasons = new ArrayList<Season>();
	List<TvSeries> tvSeries = new ArrayList<TvSeries>();
}
