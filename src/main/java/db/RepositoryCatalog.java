package db;

import repos.HsqlActorRepository;

public interface RepositoryCatalog {
	
	ActorRepository actors();
	DirectorRepository directors();
//	TvSeriesRepository tvSeries();
//	SeasonRepository seasons();
	EpisodeRepository episodes();
}
