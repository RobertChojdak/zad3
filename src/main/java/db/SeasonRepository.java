package db;

import java.util.List;

import model.Director;
import model.Episode;
import model.Season;

public interface SeasonRepository extends IRepository<Season> {
	List<Season> withSeasonNumber();
	List<Season> withYearOfRelease();
}
