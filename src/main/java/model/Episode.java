package model;
import java.time.LocalDate;


public class Episode implements ModelObject {
	
	private int id;
	private String name;
	private LocalDate releaseDate;
	private int episodeNumber;
	private int duration;
	
	
	public Episode(){
	}
	public Episode(String name,LocalDate releaseDate, int episodeNumber, int duration) {
	 this.name = name;
	 this.releaseDate = releaseDate;
	 this.episodeNumber = episodeNumber;
	 this.duration = duration;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public LocalDate getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(LocalDate releaseDate) {
		this.releaseDate = releaseDate;
	}
	public int getEpisodeNumber() {
		return episodeNumber;
	}
	public void setEpisodeNumber(int episodeNumber) {
		this.episodeNumber = episodeNumber;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	@Override
	public String toString() {
		return "Episode name: " + this.getName() + "\n" + "Episode number: " + this.getEpisodeNumber() + "\n" + "Episode relase date: " + this.getReleaseDate() + "\n" + "E[isode duration: " + this.getDuration() + "\n";
	}
	
}
