package model;

import java.time.LocalDate;


public class Director implements ModelObject {
	
	private int id;
	private String name;
	private LocalDate dateOfBirth;
	private String biography;
	
	
	public Director() {
	}
	public Director(String name, LocalDate dateOfBirth, String biography) {
		this.name = name;
		this.dateOfBirth = dateOfBirth;
		this.biography = biography;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getBiography() {
		return biography;
	}
	public void setBiography(String biography) {
		this.biography = biography;
	}
	
}
