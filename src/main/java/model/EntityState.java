package model;

public enum EntityState {
	New, Changed, Unchanged, Deleted
}
