package model;
import java.util.ArrayList;
import java.util.List;


public class TvSeries implements ModelObject {
	
	private int id;
	private String name;
	private List<Season> seasons = new ArrayList<Season>();
	
	
	public TvSeries(){
	}
	public TvSeries(String name) {
		this.name = name;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void addSeasons(Season season) {
		this.seasons.add(season);
	}
	public List<Season> getSeasons() {
		return seasons;
	}

	@Override
	public String toString() {
		return "Series name: " + this.getName() + "\n";
	}

}
