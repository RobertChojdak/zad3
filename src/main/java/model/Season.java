package model;
import java.util.ArrayList;
import java.util.List;


public class Season implements ModelObject {
	
	private int id;
	private int seasonNumber;
	private int yearOfRelease;
	private List<Episode> episodes = new ArrayList<Episode>();
	
	
	public Season(){
	}
	public Season(int seasonNumber, int yearOfRelease) {
		this.seasonNumber = seasonNumber;
		this.yearOfRelease = yearOfRelease;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSeasonNumber() {
		return seasonNumber;
	}
	public void setSeasonNumber(int seasonNumber) {
		this.seasonNumber = seasonNumber;
	}
	public int getYearOfRelease() {
		return yearOfRelease;
	}
	public void setYearOfRelease(int yearOfRelease) {
		this.yearOfRelease = yearOfRelease;
	}
	public void addEpisodes(Episode episode) {
		episodes.add(episode);
	}
	public List<Episode> getEpisodes() {
		return episodes;
	}

	@Override
	public String toString() {
		return "Season number: " + this.getSeasonNumber() + "\n" + "Season Year of Release: " + this.getYearOfRelease() + "\n";
	}
}
