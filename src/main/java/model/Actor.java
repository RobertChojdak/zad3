package model;
import java.time.LocalDate;


public class Actor implements ModelObject {
		
	private int id;
	private String name;
	private LocalDate dateOfBirth;
	private String biography;
	
	
	public Actor(){
	}
	public Actor(String name, LocalDate dateOfBirth, String biography) {
		this.name = name;
		this.dateOfBirth = dateOfBirth;
		this.biography = biography;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getBiography() {
		return biography;
	}
	public void setBiography(String biography) {
		this.biography = biography;
	}
	
	@Override
	public String toString() {
		return "Id: " + this.getId() + "\n" + "Actor name: " + this.getName() + "\n" + "Actor DOB: " + this.getDateOfBirth() + "\n" + "Actor biography: " + this.getBiography() + "\n";
	}
	
}
